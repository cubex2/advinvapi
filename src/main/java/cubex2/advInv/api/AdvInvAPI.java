package cubex2.advInv.api;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;

import java.lang.reflect.Method;

public class AdvInvAPI
{
    static Method get;

    /**
     * Gets the extended inventory for the given player
     */
    public static IInventory getExtendedInventory(EntityPlayer player)
    {
        IInventory inv = null;

        try
        {
            if (get == null)
            {
                Class<?> clazz = Class.forName("cubex2.advInv.data.ExtendedPlayerInv");
                get = clazz.getMethod("get", EntityPlayer.class);
            }

            inv = (IInventory) get.invoke(null, player);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return inv;
    }
}
